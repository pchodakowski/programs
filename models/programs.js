const mongoose = require("mongoose");

const programSchema = new mongoose.Schema({
    name: String,
    conditions: String, //new, used
    years: String,
    hours: String,
    financeProduct: String, //is there a list of these somewhere - should they be in their own schema?
    use: String, //farm, personal, etc.
    rate: String, //the rate for the program
    asset: [
        {
            manufacturer: String,
            model: String
        }
    ]
});

module.exports = mongoose.model("Program", programSchema);